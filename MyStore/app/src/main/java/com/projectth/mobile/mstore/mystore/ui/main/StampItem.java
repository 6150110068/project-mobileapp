package com.projectth.mobile.mstore.mystore.ui.main;

public class StampItem {
    public int id = 0;
    public String title;
    public String desc;
    public String date;


    public StampItem(int id, String title, String desc,String date) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.date = date;
    }
}
