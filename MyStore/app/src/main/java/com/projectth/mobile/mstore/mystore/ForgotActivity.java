package com.projectth.mobile.mstore.mystore;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ForgotActivity extends AppCompatActivity {

    Button btnSend;
    EditText edtEmail;
    EditText edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);


        edtEmail = (EditText) findViewById(R.id.edtEmail);
        btnSend = (Button) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DoForgotPassword doLogin = new DoForgotPassword();
//                doLogin.email = edtEmail.getText().toString();
//                doLogin.execute();
            }
        });
    }

    private class DoForgotPassword extends AsyncTask<Void, Void, String> {
        String postUrl;
        String email="";

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            postUrl  = App.getInstance().SERVER_URI + App.getInstance().CUST_PROFILE;
        }

        protected String doInBackground(Void... urls)   {
            client.setConnectTimeout(30, TimeUnit.SECONDS);
            client.setReadTimeout(30,TimeUnit.SECONDS);
            String result = null;
            try {
                RequestBody formBody = new FormEncodingBuilder()

                        .add("email",email)

                        .build();
                Request request = new Request.Builder()
                        .url(postUrl)
                        .post(formBody)
                        .build();


                Response response = client.newCall(request).execute();
                return response.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        protected void onPostExecute(String result)  {

            try {
                JSONArray jsonObj = new JSONArray(result);
                if(jsonObj.length() == 1 && jsonObj.getJSONObject(0).getString("success").equals("true")) {
                    new AlertDialog.Builder(ForgotActivity.this)
                            .setTitle("ข้อมูล")
                            .setMessage("ทำรายการเรียบร้อย")
                            .setNeutralButton("ปิด",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();

                                        }
                                    })

                            .show();
                }else{

                    new AlertDialog.Builder(ForgotActivity.this)
                            .setTitle("แจ้งเตือน")
                            .setMessage("ไม่สามารถแสดงรายการข้อมูลได้ โปรดตรวจสอบการเชื่อมต่อ Internet")
                            .setNeutralButton("ปิด",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();

                                        }
                                    })

                            .show();

                }

            } catch (Exception e) {
                e.printStackTrace();

                new AlertDialog.Builder(ForgotActivity.this)
                        .setTitle("แจ้งเตือน")
                        .setMessage("ไม่สามารถแสดงรายการข้อมูลได้ โปรดตรวจสอบการเชื่อมต่อ Internet")
                        .setNeutralButton("ปิด",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();


                                    }
                                })

                        .show();
            }

        }

        OkHttpClient client = new OkHttpClient();

    }
}