package com.projectth.mobile.mstore.mystore;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.projectth.mobile.mstore.mystore.ui.main.StampAdapter;
import com.projectth.mobile.mstore.mystore.ui.main.StampItem;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class PaybackStampActivity extends AppCompatActivity {
    ListView lstView;
    GetPaybackStamp getPaybackStamp;
    StampAdapter stampAdapter;
    ArrayList<StampItem> arrStamp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paybackstamp);

        arrStamp = new ArrayList<StampItem>();
        arrStamp.add(new StampItem(1,"title 1","desc 1","2020-10-20"));
        arrStamp.add(new StampItem(2,"title 2","desc 2","2020-10-20"));
        arrStamp.add(new StampItem(3,"title 3","desc 3","2020-10-20"));
        arrStamp.add(new StampItem(4,"title 4","desc 4","2020-10-20"));
        arrStamp.add(new StampItem(5,"title 5","desc 5","2020-10-20"));
        arrStamp.add(new StampItem(6,"title 6","desc 6","2020-10-20"));
        stampAdapter = new StampAdapter(this, R.layout.list_item,arrStamp);
        lstView = (ListView)findViewById(R.id.lstView);
        lstView.setAdapter(stampAdapter);
        stampAdapter.notifyDataSetChanged();


        getPaybackStamp = new GetPaybackStamp();
        //getPaybackStamp.execute();

    }


    private class GetPaybackStamp extends AsyncTask<Void, Void, String> {
        String postUrl;
        String u;

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            postUrl  = App.getInstance().SERVER_URI + App.getInstance().LIST_SHOP;
        }

        protected String doInBackground(Void... urls)   {
            client.setConnectTimeout(30, TimeUnit.SECONDS);
            client.setReadTimeout(30,TimeUnit.SECONDS);
            String result = null;
            try {
                Request request = new Request.Builder()
                        .url(postUrl)
                        .build();
                Response response = client.newCall(request).execute();
                return response.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        protected void onPostExecute(String result)  {

            try {
                JSONArray jsonObj = new JSONArray(result);
                if(jsonObj.length() > 0) {
                    arrStamp.clear();
                    for(int i = 0; i < jsonObj.length() ; i++){
                        JSONObject itm = jsonObj.getJSONObject(i);
                        arrStamp.add(new StampItem(itm.getInt("shop_id"),itm.getString("shop_name"),itm.getString("address"),itm.getString("date")));
                    }

                    stampAdapter.notifyDataSetChanged();
                }else{

                    new AlertDialog.Builder(PaybackStampActivity.this)
                            .setTitle("แจ้งเตือน")
                            .setMessage("ไม่สามารถแสดงรายการข้อมูลได้ โปรดตรวจสอบการเชื่อมต่อ Internet")
                            .setNeutralButton("ปิด",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();

                                        }
                                    })

                            .show();

                }

            } catch (Exception e) {
                e.printStackTrace();

                new AlertDialog.Builder(PaybackStampActivity.this)
                        .setTitle("แจ้งเตือน")
                        .setMessage("ไม่สามารถแสดงรายการข้อมูลได้ โปรดตรวจสอบการเชื่อมต่อ Internet")
                        .setNeutralButton("ปิด",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();


                                    }
                                })

                        .show();
            }

        }

        OkHttpClient client = new OkHttpClient();

    }
}