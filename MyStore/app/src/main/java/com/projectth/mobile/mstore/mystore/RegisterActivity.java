package com.projectth.mobile.mstore.mystore;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.projectth.mobile.mstore.mystore.ui.main.StampItem;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class RegisterActivity extends AppCompatActivity {

    Button btnRegister;
    EditText edtEmail;
    EditText edtCustName;
    EditText edtTel;
    EditText edtAddress;

    EditText edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtCustName = (EditText) findViewById(R.id.edtCustName);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtAddress = (EditText) findViewById(R.id.edtAddress);
        edtTel = (EditText) findViewById(R.id.edtTel);
        btnRegister = (Button) findViewById(R.id.btnRegister);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DoRegister doLogin = new DoRegister();
                doLogin.cust_name = edtCustName.getText().toString();
                doLogin.email = edtEmail.getText().toString();
                doLogin.pword = edtPassword.getText().toString();
                doLogin.tel = edtTel.getText().toString();
                doLogin.address = edtAddress.getText().toString();
                doLogin.execute();
            }
        });
    }

    private class DoRegister extends AsyncTask<Void, Void, String> {
        String postUrl;
        String pword="",cust_name="",email="",tel="",address="";

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            postUrl  = App.getInstance().SERVER_URI + App.getInstance().REGIST_CUST;
        }

        protected String doInBackground(Void... urls)   {
            client.setConnectTimeout(30, TimeUnit.SECONDS);
            client.setReadTimeout(30,TimeUnit.SECONDS);
            String result = null;
            try {
                RequestBody formBody = new FormEncodingBuilder()

                        .add("customer_name",cust_name)
                        .add("email",email)
                        .add("username",email)
                        .add("password",pword)
                        .add("phone_number",tel)
                        .add("address",address)

                        .build();
                Request request = new Request.Builder()
                        .url(postUrl)
                        .post(formBody)
                        .build();


                Response response = client.newCall(request).execute();
                return response.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        protected void onPostExecute(String result)  {

            try {
                JSONArray jsonObj = new JSONArray(result);
                if(jsonObj.length() == 1 && jsonObj.getJSONObject(0).getString("success").equals("true")) {
                    new AlertDialog.Builder(RegisterActivity.this)
                            .setTitle("ข้อมูล")
                            .setMessage("ลงทะเบียนร้านค้าแล้ว")
                            .setNeutralButton("ปิด",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();

                                        }
                                    })

                            .show();
                }else{

                    new AlertDialog.Builder(RegisterActivity.this)
                            .setTitle("แจ้งเตือน")
                            .setMessage("ไม่สามารถแสดงรายการข้อมูลได้ โปรดตรวจสอบการเชื่อมต่อ Internet")
                            .setNeutralButton("ปิด",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();

                                        }
                                    })

                            .show();

                }

            } catch (Exception e) {
                e.printStackTrace();

                new AlertDialog.Builder(RegisterActivity.this)
                        .setTitle("แจ้งเตือน")
                        .setMessage("ไม่สามารถแสดงรายการข้อมูลได้ โปรดตรวจสอบการเชื่อมต่อ Internet")
                        .setNeutralButton("ปิด",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();


                                    }
                                })

                        .show();
            }

        }

        OkHttpClient client = new OkHttpClient();

    }
}