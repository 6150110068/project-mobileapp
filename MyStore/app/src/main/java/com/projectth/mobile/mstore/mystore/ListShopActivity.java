package com.projectth.mobile.mstore.mystore;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.projectth.mobile.mstore.mystore.ui.main.StampAdapter;
import com.projectth.mobile.mstore.mystore.ui.main.StampItem;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class ListShopActivity extends AppCompatActivity {
    ListView lstView;
    GetListShop getlistShop;
    StampAdapter stampAdapter;
    ArrayList<StampItem> arrStamp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paybackstamp);

        arrStamp = new ArrayList<StampItem>();
        stampAdapter = new StampAdapter(this, R.layout.list_item,arrStamp);
        lstView = (ListView)findViewById(R.id.lstView);
        lstView.setAdapter(stampAdapter);


        getlistShop = new GetListShop();
        getlistShop.execute();

    }

    /**
     * Agent info
     */
    private class GetListShop extends AsyncTask<Void, Void, String> {
        String postUrl;
        String u;

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            postUrl  = App.getInstance().SERVER_URI + App.getInstance().LIST_SHOP;
        }

        protected String doInBackground(Void... urls)   {
            client.setConnectTimeout(30, TimeUnit.SECONDS);
            client.setReadTimeout(30,TimeUnit.SECONDS);
            String result = null;
            try {
                Request request = new Request.Builder()
                        .url(postUrl)
                        .build();
                Response response = client.newCall(request).execute();
                return response.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        protected void onPostExecute(String result)  {

            try {
                JSONArray jsonObj = new JSONArray(result);
                if(jsonObj.length() > 0) {
                    arrStamp.clear();
                    for(int i = 0; i < jsonObj.length() ; i++){
                        JSONObject itm = jsonObj.getJSONObject(i);
                        arrStamp.add(new StampItem(itm.getInt("shop_id"),itm.getString("shop_name"),itm.getString("address"),itm.getString("date")));
                    }

                    stampAdapter.notifyDataSetChanged();
                }else{

                    new AlertDialog.Builder(ListShopActivity.this)
                            .setTitle("แจ้งเตือน")
                            .setMessage("ไม่สามารถแสดงรายการข้อมูลได้ โปรดตรวจสอบการเชื่อมต่อ Internet")
                            .setNeutralButton("ปิด",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();

                                        }
                                    })

                            .show();

                }

            } catch (Exception e) {
                e.printStackTrace();

                new AlertDialog.Builder(ListShopActivity.this)
                        .setTitle("แจ้งเตือน")
                        .setMessage("ไม่สามารถแสดงรายการข้อมูลได้ โปรดตรวจสอบการเชื่อมต่อ Internet")
                        .setNeutralButton("ปิด",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();


                                    }
                                })

                        .show();
            }

        }

        OkHttpClient client = new OkHttpClient();

    } // .End AgentInfo
}