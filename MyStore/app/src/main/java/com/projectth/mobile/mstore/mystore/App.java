package com.projectth.mobile.mstore.mystore;

import org.json.JSONObject;

import java.math.BigInteger;

/**
 * Created by rewat on 2/6/2560.
 */

public class App {

    public static String STAY_LOGIN = "1";

    static private App object;
    public static final String MY_PREFS = "myworld3g_projectth_myag";

    public String SERVER_URI =  "https://itlearningcenters.com/android/project0810/";
    public String REGIST_CUST =  "regist_cust.php";
    public String CUST_LOGIN =  "cust_login.php";
    public String CUST_PROFILE =  "cust_profile.php";
    public String REGIST_SHOP =  "regist_shop.php";
    public String LIST_SHOP =  "list_shops.php";


    static public App getInstance(){
        if(object == null){
            object = new App();
        }

        return object;
    } // .End getInstance

} // .End class App