package com.projectth.mobile.mstore.mystore.ui.main;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.zxing.integration.android.IntentIntegrator;
import com.projectth.mobile.mstore.mystore.ListShopActivity;
import com.projectth.mobile.mstore.mystore.MainActivity;
import com.projectth.mobile.mstore.mystore.PaybackStampActivity;
import com.projectth.mobile.mstore.mystore.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final int SCANNER_REQUEST_CODE = 1001;

    public static PlaceholderFragment newInstance(int index) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_main, container, false);

        ImageButton imbHome, imbScanner, imbBookmark;
        imbHome = (ImageButton) root.findViewById(R.id.imbHome);
        imbScanner = (ImageButton) root.findViewById(R.id.imbScanner);
        imbBookmark = (ImageButton) root.findViewById(R.id.imbBookmark);

        imbHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PlaceholderFragment.this.getContext(), ListShopActivity.class);
                startActivity(intent);
            }
        });

        imbBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PlaceholderFragment.this.getContext(), PaybackStampActivity.class);
                startActivity(intent);
            }
        });

        imbScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        getActivity().requestPermissions(new String [] { Manifest.permission.CAMERA },1000);
                    }

                    return;
                }


                IntentIntegrator integrator = new IntentIntegrator(getActivity());


                integrator.setCameraId(0);
                integrator.setBarcodeImageEnabled(true);
                integrator.setBeepEnabled(true);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.CODE_128);
                //integrator.setRequestCode(SCANNER_REQUEST_CODE_TEL);
                //integrator.initiateScan();
                Intent intent = integrator.createScanIntent();
                startActivityForResult(intent,SCANNER_REQUEST_CODE);
            }
        });

        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {


        if (requestCode == SCANNER_REQUEST_CODE) {

            // Handle scan intent


            if (Activity.RESULT_OK == resultCode) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                Toast.makeText(getActivity(), "COCDE : " + contents, Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_LONG).show();
            }


        } // .End else if


        super.onActivityResult(requestCode, resultCode, intent);

    } // .End OnActivityForResult()
}