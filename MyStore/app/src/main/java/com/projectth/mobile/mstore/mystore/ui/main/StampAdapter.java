package com.projectth.mobile.mstore.mystore.ui.main;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.projectth.mobile.mstore.mystore.R;

import java.util.ArrayList;



public class StampAdapter extends ArrayAdapter<StampItem> {
    ArrayList<StampItem> m_al;
    Context ctx;
    int m_resource;
    public StampAdapter(@NonNull Context context, int resource, ArrayList<StampItem> al) {
        super(context, resource,al);
        this.m_resource = resource;
        ctx = context;
        m_al = al;
    }

    @Nullable
    @Override
    public StampItem getItem(int position) {
        return m_al.get(position);
    }

    // ...
    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        // ...


        // Access the row position here to get the correct data item
        StampItem stampItem = getItem(position);
        // Do what you want here...



        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(m_resource, parent, false);
        }

        TextView txvTitle = convertView.findViewById(R.id.txvTitle);
        TextView txvDesc = convertView.findViewById(R.id.txvDesc);

        txvTitle.setText(stampItem.title);
        txvDesc.setText(stampItem.desc);

        // ... other view population as needed...
        // Return the completed view
        return convertView;
    }
}